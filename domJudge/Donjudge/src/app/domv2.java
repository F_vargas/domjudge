package app;

import java.util.Scanner;

/**
 * Donjudge
 * @version V-2
 * @author Fvrgas
 * @since 29-02-2020
 */


 /**En la class solo encontramos el scanner */
public class domv2 {
    static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        /** NCv es el valor por numero de caballos del coche */
        int NCV = 0;
        /** Es el valor por el porcentaje de caballos que quiere subbir */
        int PCV = 0;
        int Nvueltas = 0;
        Nvueltas = reader.nextInt();
        for (int i = 0; i < Nvueltas; i++) {
            NCV = pedirNumeroCaballos();
            PCV = pedirPorcentajecaballos();
            int res = validaciones(NCV, PCV);
            if (res != 0) {
                System.out.print(res);
            } else {
                System.out.println("Petaste el motor!");
            }
        }

    }
/**
 * En este metodo le pedimos al usuario el numero de caballos que tiene su coche
 * @return Devuelve el valor introducido por el usuario.
 */
     static int pedirNumeroCaballos() {
        return reader.nextInt();
    }
/**
 * En este metodo le pedimos al usuario el porcentaje de cv que quiere subir.
 * @return Devuelve El valor introducido por el usuario.
 */
     static int pedirPorcentajecaballos() {
        return reader.nextInt();
    }

 
/**
 * Validaciones es la parte la cual miramos los valores introducidos por el usuario y hace la validacion para llamar a la funcion que le toca.
 * @param NCV Este paramentro de entarda son el numero de caballos que tien el coche.
 * @param PCV Este parametro de entrada es el porcentaje de caballos que quiere subir.
 * @return Develve el resultado de la funcion que le ha tocado hacer por los valores introducidos 
 */
    static int validaciones(int NCV, int PCV) {
        if (PCV < 60 && NCV < 400) {
            return calcularmenosde400cv(NCV, PCV);
        } else if (PCV<100 && NCV >= 400) {
            return calcularmasde400cv(NCV, PCV);
        } else if (PCV == 100) {
            return 0;
        } else {
            return 0;
        }

    }
 /**Es la funcion donde calculamos los coches que teinen menos de 400 cv y el porcentaje no supera el 60%
  * 
  * @param NCV Este paramentro de entarda son el numero de caballos que tien el coche.
  * @param PCV Este parametro de entrada es el porcentaje de caballos que quiere subir.
  * @return Devuelve el calculo del porcentaje y los caballos.
  */
   static int calcularmenosde400cv(int NCV, int PCV) {
        int res = (NCV * PCV) / 100;
        res = NCV + res;

        return res;
    }
 /**Es la funcion donde calculamos los coches que teinen mas de 400 cv y el porcentaje no supera el 100%
  * 
  * @param NCV Este paramentro de entarda son el numero de caballos que tien el coche.
  * @param PCV Este parametro de entrada es el porcentaje de caballos que quiere subir.
  * @return Devuelve el calculo del porcentaje y los caballos.
  */
    static int calcularmasde400cv(int NCV, int PCV) {
        int res = (NCV * PCV) / 100;
        res = NCV + res;

        return res;
    }

}